require_relative './test_helper'

describe Dugovanja do

  it "should retrieve debtors from XML file" do
    skip
    file = File.open("attachments/tif dugovanja fizicka l..xml")
    debtors = Dugovanja.debtors_from_xml_file(file, false)
    assert_equal 2398, debtors.size
  end

  it "should parse all XML files" do
    skip
    debtors = Dugovanja.parse_all_xml_files
    assert_equal 5659, debtors.size
  end

  it "should parse all XML files and serialize debtors" do
    skip
    Dugovanja.parse_all_xml_files_and_serialize_debtors
  end

  it "should match debtors in excel file" do
    skip
    debtors = Dugovanja.deserialize_debtors
    matches = Dugovanja.match_debtors_in_excel_file("attachments/fwdoptinaagubicadugovanja/bliznak 15.07.'14..xls", debtors, true)
    assert_equal 67, matches.size
  end

  it "should write matches to excel to a duplication excel file" do
    skip
    debtors = Dugovanja.deserialize_debtors
    matches = Dugovanja.match_debtors_in_excel_file("attachments/fwdoptinaagubicadugovanja/bliznak 15.07.'14..xls", debtors, true)
    Dugovanja.write_debtor_matches_to_a_duplicate_excel_file("attachments/fwdoptinaagubicadugovanja/bliznak 15.07.'14..xls", matches)
  end

  it "should write matches to all excel files - new files reside in generated folder" do
    Dugovanja.parse_all_xml_files_and_serialize_debtors
    xls_files = Dir.glob('attachments/**/*.xls')
    debtors = Dugovanja.deserialize_debtors
    xls_files.each do |xls_file|
      puts xls_file
      matches = Dugovanja.match_debtors_in_excel_file(xls_file, debtors, false)
      Dugovanja.write_debtor_matches_to_a_duplicate_excel_file(xls_file, matches)
    end
  end

end