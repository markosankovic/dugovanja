# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'dugovanja/version'

Gem::Specification.new do |spec|
  spec.name = "dugovanja"
  spec.version = Dugovanja::VERSION
  spec.authors = ["Marko Sankovi\304\207"]
  spec.email = ["sankovic.marko@gmail.com"]
  spec.description = %q{Kopiranje JMBG i telefona sa starih spiskova dugovanja na nove spiskove}
  spec.summary = %q{Kopiranje JMBG i telefona sa starih spiskova dugovanja na nove spiskove}
  spec.homepage = ""
  spec.license = "MIT"

  spec.files = `git ls-files`.split($/)
  spec.executables = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"

  spec.add_development_dependency "minitest", "~> 5.4.0"

  spec.add_runtime_dependency "nokogiri"
  spec.add_runtime_dependency "roo"
  spec.add_runtime_dependency "axlsx"
end
