require "dugovanja/version"
require "nokogiri"
require "yaml"
require "roo"
require "axlsx"
require "pathname"
require "fileutils"

module Dugovanja

  def self.debtors_from_xml_file(file, verbose = true)

    doc = Nokogiri::XML(file)

    normalized_place_names = {
        'BATUÆA' => 'BATUŠA',
        'VELIKO CRNIÆE' => 'VELIKO CRNIĆE',
        'MALO CRNIÆE' => 'MALO CRNIĆE',
        'KALIÆTE' => 'KALIŠTE',
        'JOÆANICA' => 'JOŠANICA',
        'SELIÆTE' => 'SELIŠTE'
    }

    text_nodes = doc.xpath("//text")

    debtors = []; debtor = {}; place = nil
    text_nodes.each do |node|

      if node['left'] == '21'
        match = node.text.match /^Mesto:\d+\s+(.*)$/i
        if match
          place = match[1]
          if normalized_place_names[place]
            place = normalized_place_names[place]
          end
        end
      end

      if node['left'] == '115' and node.text != 'Naziv'
        debtors << debtor if debtor
        debtor = {name: node.text.strip, place: place}
      end

      if node['left'] == '377' and node.text != 'Pib/Jmbg'
        debtor[:pin] = node.text
      end

      if node['left'] == '479' and node.text != 'Telefon'
        debtor[:telephone] = node.text
      end

    end

    debtors << debtor

    if verbose
      puts debtors
      puts debtors.size
    end

    debtors
  end

  def self.parse_all_xml_files
    files = [
        File.open("attachments/tif dugovanja fizicka l..xml"),
        File.open("attachments/fwdoptinamalocrniejmbgbrojevitelefona/stanje redovna - fizicka.xml"),
        File.open("attachments/fwdoptinamalocrniejmbgbrojevitelefona/stanje sporna - fizicka.xml")
    ]

    debtors = []
    files.each do |file|
      debtors.concat(self.debtors_from_xml_file(file, false))
    end

    debtors
  end

  def self.serialize_debtors(debtors)
    File.open("generated/debtors.yaml", 'w') { |file| file.write(YAML::dump(debtors)) }
  end

  def self.parse_all_xml_files_and_serialize_debtors
    debtors = self.parse_all_xml_files
    self.serialize_debtors(debtors)
  end

  def self.deserialize_debtors
    YAML.load_file("generated/debtors.yaml")
  end

  def self.match_debtors_in_excel_file(file, debtors, verbose = true)

    rows = Roo::Excel.new(file)
    unmatched_rows = rows.to_a.map { |row| row[0].to_i }
    matches = {}

    rows.each_with_index do |row, index|
      next unless row[3]
      r = Regexp.new("^#{Regexp.escape(row[3])}(.*)$")
      debtors.each do |debtor|
        if debtor[:name]
          debtor_name = debtor[:name].gsub(/\s+/, ' ') # debtor[:name].gsub('*', '')
          if row[1] == debtor[:place] && debtor_name.match(r)
            # TODO Found match, send to another method for packaging into CSV
            matches[row[0].to_i] = debtor
            unmatched_rows.delete_if { |urow| urow == row[0].to_i }
            break
          end
        end
      end
    end

    if verbose
      puts matches
      puts
      puts "UNMATCHED ROWS: " + unmatched_rows.join(' ')
    end

    matches
  end

  def self.write_debtor_matches_to_a_duplicate_excel_file(filename, matches)

    excel_rows = Roo::Excel.new(filename)
    rows = excel_rows.to_a
    rows.delete(rows.first)
    rows.delete(rows.last)

    new_filename = filename.gsub("attachments", "generated") # "generated/#{Pathname.new(filename).basename.to_s}"

    p = Axlsx::Package.new
    wb = p.workbook

    wb.add_worksheet(name: "Sheet1") do |sheet|

      # wb.styles.fonts.first.name = "Times New Roman"
      # wb.styles.fonts.first.sz = 10

      price_column_style = wb.styles.add_style :format_code => "#,##0.00"

      sheet.add_row ["Nr. crt.", "Mesto", "Šifra klijenta", "Klijent", "JMBG", "Telefon", "Broj redova", "Fakturisano", "Incasari aferente perioadei solicitate"]

      rows.each_with_index do |row, index|

        debtor = matches[row[0].to_i]

        if debtor
          sheet_row = [row[0], row[1], row[2], row[3]]
          sheet_row[4] = debtor[:pin] || ''
          sheet_row[5] = debtor[:telephone] || ''
          sheet_row[6] = row[4]
          sheet_row[7] = row[5]
          sheet_row[8] = row[6]
        else
          sheet_row = row
          sheet_row.insert(4, '')
          sheet_row.insert(5, '')
          sheet_row << '*'
        end

        sheet.add_row sheet_row, style: [nil, nil, nil, nil, nil, nil, nil, price_column_style, price_column_style]
      end

      sheet.add_row ["TOTAL GENERAL:", nil, nil, nil, nil, nil, nil, "=SUM(H2:H#{sheet.rows.size})", "=SUM(I2:I#{sheet.rows.size})"], style: [nil, nil, nil, nil, nil, nil, nil, price_column_style, price_column_style]
      sheet.merge_cells("A#{sheet.rows.size}:G#{sheet.rows.size}")
    end

    dirname = File.dirname(new_filename)
    unless File.directory?(dirname)
      FileUtils.mkdir_p(dirname)
    end

    p.serialize new_filename
  end

end
