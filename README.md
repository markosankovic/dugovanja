# Dugovanja

Kopiranje JMBG i telefona sa starih spiskova dugovanja na nove spiskove.

## Installation

Add this line to your application's Gemfile:

    gem 'dugovanja'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install dugovanja

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
